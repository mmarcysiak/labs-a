/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabA.Contracts;
import javax.swing.JPanel;

public interface Display {  
    
    JPanel getPanel();
    
    void setText(String text);
    
}